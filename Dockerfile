FROM rust:slim

RUN apt-get update \
    && apt-get install -qqy --no-install-recommends wget openssh-client \
    && apt-get clean

RUN cargo install static-compress
